## SRWM

SRWM is a simple stacking window manager, tiling will be added in the future.
I've taken a lot of inspiration from 2bwm along with FrankenWM and Herbstluftwm.
It seems like a lot of minimal window managers are doing similar things these
days...and this one is no different.  I pretty much wrote this to learn
XCB and X programming to keep me busy while on university holidays.

# TODO
* basic tiling
* config file - started 
* multiple workspaces - added, only have keybindings for dfirst 2
* delete windows properly
* ~refactor move function~ DONE
