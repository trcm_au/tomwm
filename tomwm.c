/* SRWM is a simple window manager, yeah I know its a lot like the rest of them out there I pretty much wrote it */
/* to try and understand X programming.  It's more difficult than I thought it would be. */
/* I took a bit of inspiration from 2bwm along with Herbstluft and frankenWM */

#include <stdlib.h>
#include <stdio.h>
#include <err.h>
#include <stdarg.h>
#include <stdbool.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <X11/keysym.h>
#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>
#include <xcb/xcb_icccm.h>
#include <xcb/xcb_keysyms.h>
#include <xcb/xcb_ewmh.h>

#include "config.h"
#include "dbg.h"

#if 1
#define DEBUG(x) puts(x);
#define DEBUGP(x, ...) printf(x, ##__VA_ARGS__);
#endif

typedef struct {
  int mod;
  xcb_keysym_t sym;
} key;

/* a client is a wrapper to a window that additionally
 * holds some properties for that window
 *
 * next        - the client after this one, or NULL if the current is the last
 *               client
 * isurgent    - set when the window received an urgent hint
 * istransient - set when the window is transient
 * isfullscrn  - set when the window is fullscreen
 * isfloating  - set when the window is floating
 * win         - the window this client is representing
 *
 * istransient is separate from isfloating as floating window can be reset
 * to their tiling positions, while the transients will always be floating
 */
typedef struct client
{
  struct client *next;
  struct client *prev;
  int id, x , y, width, height;
  /* bool isurgent, istransient, isfullscrn, isfloating; */
  xcb_window_t win;
} client;

/* properties of each desktop
 * master_size  - the size of the master window
 * mode         - the desktop's tiling layout mode
 * growth       - growth factor of the first stack window
 * head         - the start of the client list
 * current      - the currently highlighted window
 * prevfocus    - the client that previously had focus
 * showpanel    - the visibility status of the panel
 */
typedef struct
{
  int wins;
  /* int mode, growth, gaps; */
  /* float master_size; */
  client *head, *current, *prevfocus, *end;
  /* bool showpanel, stackinvert; */
} desktop;

uint32_t mask = 0;

// width and height of the screen
int ww, wh; 
int numWins = 0;

const int NUMBER_OF_DESKTOPS = 5;
/* the current mouse cursor location */
int point_x, point_y;

uint32_t values[3];
xcb_connection_t *conn;
xcb_screen_t *screen;
xcb_drawable_t win;
xcb_drawable_t root;
xcb_generic_event_t *ev;
xcb_get_geometry_reply_t *geom;
xcb_window_t window;
xcb_window_t current;
xcb_timestamp_t currentTime;

/* client *head, *end; */
/* client *windowList; */
client *currentClient;

desktop *currentDesk;
int currentDeskNum;
desktop desktops[NUMBER_OF_DESKTOPS];

//this is set to one if the desktop is just changing so the windows won't be added again
int change = 0;

static xcb_keycode_t *xcb_get_keycords(xcb_keysym_t keysym);
void nextwin();
void prevwin();
void focusOnCurrent();
int destroyWin(client *current);
void changeDesktop(int deskNum);
void saveGeometry(client *curr);
int setup();
void setupKeys();
void resizeScale(xcb_window_t win, int direction);
void move(xcb_window_t win, int xy, int step, int direction);
void resize(xcb_window_t win, int xy, int step, int direction);
void tile(xcb_window_t win, int lr);
void fullcreenWindow(xcb_window_t win);
void addwin(xcb_window_t win);
void maprequest(xcb_generic_event_t *e);


// this is run to spawn external problems, mainly for bindings keys to shortcuts
int spawn(char *com)
{
  debug("spawn");
  if (fork() == 0)
    {
      char** out;
      setsid();
      execvp(com, out);
      exit(EXIT_SUCCESS);
    }
  return 0;
}

/* wrapper to get xcb keycodes from keysymbol */
static xcb_keycode_t *xcb_get_keycodes(xcb_keysym_t keysym)
{
  xcb_key_symbols_t *keysyms;
  xcb_keycode_t     *keycode;

  if (!(keysyms = xcb_key_symbols_alloc(conn)))
    return NULL;
  keycode = xcb_key_symbols_get_keycode(keysyms, keysym);
  xcb_key_symbols_free(keysyms);

  return keycode;
}

void xcb_raise_window(xcb_connection_t *conn, xcb_window_t win)
{
  uint32_t arg[1] = { XCB_STACK_MODE_ABOVE };
  xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_STACK_MODE, arg);
}

void xcb_lower_window(xcb_connection_t *conn, xcb_window_t win)
{
  uint32_t arg[1] = { XCB_STACK_MODE_BELOW };
  xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_STACK_MODE, arg);
}

void move(xcb_window_t win, int xy, int step, int direction)
{
  uint32_t args[1];
  xcb_get_geometry_reply_t *geom = xcb_get_geometry_reply(conn,	xcb_get_geometry(conn, win), NULL);

  if (geom != NULL)
  {
    switch (xy)
    {
      // move in the x direction
    case 0:
    {
      if (direction)
      {
	args[0] = geom->x - step;
      } else
      {
	args[0] = geom->x + step;
      }
      xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_X , args);
      break;
    }
    // movei n the y direction
    case 1:
    {
      if (direction)
      {
	args[0] = geom->y - step;
      } else
      {
	args[0] = geom->y + step;
      }
      xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_Y , args);
      break;
    }
    }
    //move pointer to the middle of the window, this should get ride of the crash when the pointer leaves the current window
    xcb_warp_pointer(conn, XCB_NONE, win, 0, 0, 0, 0, (geom->width/2), (geom->height/2));
    free(geom);
    xcb_flush(conn);
  }
}

void resize(xcb_window_t win, int xy, int step, int direction)
{
  uint32_t args[1];
  xcb_get_geometry_reply_t *geom = xcb_get_geometry_reply(conn,	xcb_get_geometry(conn, win), NULL);
  if (geom != NULL)
  {
    switch(xy)
    {
      // resize in x dirextion
    case 0:
    {
      if (direction)
      {
	args[0] = geom->width - step;
      }
      else
      {
	args[0] = geom->width + step;
      }
      xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_WIDTH, args);
      break;
    }
    // resize in y direction
    case 1:
    {
      if (direction)
      {
	args[0] = geom->height - step;
      }
      else
      {
	args[0] = geom->height + step;
      }
      xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_HEIGHT, args);
      break;
    }
    //move pointer to the middle of the window, this should get ride of the crash when the pointer leaves the current window
    xcb_warp_pointer(conn, XCB_NONE, win, 0, 0, 0, 0, (geom->width/2), (geom->height/2));
    free(geom);
    xcb_flush(conn);
    }
  }
}

void resizeScale(xcb_window_t win, int direction)
{
  uint32_t args[2];
  xcb_get_geometry_reply_t *geom = xcb_get_geometry_reply(conn,	xcb_get_geometry(conn, win), NULL);
  if (geom != NULL)
  {    
    if (direction) {
      args[1] = geom->height - 10;
      args[0] = geom->width - 10;
    } else {
      args[1] = geom->height + 10;
      args[0] = geom->width + 10;
    }
    xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_WIDTH|XCB_CONFIG_WINDOW_HEIGHT, args);
    //move pointer to the middle of the window, this should get ride of the crash when the pointer leaves the current window
    xcb_warp_pointer(conn, XCB_NONE, win, 0, 0, 0, 0, (geom->width/2), (geom->height/2));
    free(geom);
    xcb_flush(conn);
  }
}

// tiles the given window to the left or right, those are the only ways I tile.
// if lr is 1 it tiles to the right, if its 0 it tiles to the left
void tile(xcb_window_t win, int lr)
{
  xcb_get_geometry_reply_t *geom = xcb_get_geometry_reply(conn, xcb_get_geometry(conn, win), NULL);
  if (geom != NULL)
  {
    if (lr)
    {
      uint32_t args[4] = { 0, 0, ww/2, wh };
      xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, args);
    }
    else
    {
      uint32_t args[4] = { ww/2, 0, ww/2, wh };
      xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, args);
    }
    xcb_warp_pointer(conn, XCB_NONE, win, 0, 0, 0, 0, (geom->width/2), (geom->height/2));
    xcb_flush(conn);
  }
}

void fullscreenWindow(xcb_window_t win)
{
  /* resize to fullscreen values X, Y, HEIGHT, WIDTH */
  const uint32_t values[4] = { 0, 0, ww, wh };
  xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_HEIGHT | XCB_CONFIG_WINDOW_WIDTH, values);
}


void nextwin()
{
  if (currentDesk->current->next == NULL)
  {
    currentDesk->current = currentDesk->head;
  } else
  {
    currentDesk->prevfocus = currentDesk->current;
    currentDesk->current = currentDesk->current->next;
  }
  focusOnCurrent();
  xcb_flush(conn);

}

void prevwin()
{
  if (currentDesk->current->prev == NULL)
  {
    currentDesk->current = currentDesk->end;
  } else
  {
    currentDesk->prevfocus = currentDesk->current;
    currentDesk->current = currentDesk->current->prev;
  }
  focusOnCurrent();
  xcb_flush(conn);
}


void focusOnCurrent()
{
  xcb_set_input_focus(conn, XCB_NONE, currentDesk->current->win, XCB_CURRENT_TIME);
  xcb_raise_window(conn, currentDesk->current->win);
}

int destroyWin(client *curr)
{
  /* fix hole in linked list */
  /* link current->next->prev to current->prev */
  /* link current->prev->next to current->next */
  /* currentDesk->current->next->prev = currentDesk->current->prev; */
  /* currentDesk->current->prev->next = currentDesk->current->next; */
  /* delete win */

  /* free current mem */
  client *temp = currentDesk->current->next;
  currentDesk->current = temp;
  xcb_destroy_window(conn, currentDesk->current->win);

  currentDesk->wins--;
  /* focusOnCurrent(); */
  return 0;
}

// this function needs to change to the specificed desktop
// the user selecgs the workspace ith the keybinding MOD4-SHIFT-#
// for the function to work, first it has to save unmap alll the windows from the 
// current workspace, then map all the ones from the new one 
void changeDesktop(int deskNum)
{
  // turn off add win
  change = 1;
  client *iter;
  if (deskNum != currentDeskNum)
  {
    // unmap all windows on current desktop
    if (currentDesk->head != NULL) {
      iter = currentDesk->head;
      for (;;)
      {
	debug("Mapping window %d", iter->id);

	xcb_unmap_window(conn, iter->win);
    
	if (iter->next == NULL)
	{
	  break;
	}
	iter = iter->next;
      }
      // switch current desktop to selected desktop
    }
    currentDeskNum = deskNum;
    currentDesk = &desktops[currentDeskNum];
    debug("changed desktop %d", currentDeskNum);
    if (currentDesk->head != NULL)
    {
      iter = currentDesk->head;
      for (;;)
      {
	debug("Mapping window %d", iter->id);
	/* uint32_t args[4] = { iter->x, iter->y, iter->width, iter->height }; */
	/* xcb_configure_window(conn, iter->win, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, args); */
	xcb_map_window(conn, iter->win);
    
	if (iter->next == NULL)
	{
	  break;
	}
	iter = iter->next;
      }
    }
  }

  // turn on adding windows
  change = 0;
}

void saveGeometry(client *curr)
{
  xcb_get_geometry_reply_t *geom = xcb_get_geometry_reply(conn, xcb_get_geometry(conn, curr->win), NULL);
  curr->width = geom->width;
  curr->height = geom->height;
  curr->x = geom->x;
  curr->y = geom->y;
  debug("Saved geometry %d %d %d %d", curr->width, curr->height, curr->x, curr->y);
}

void addwin(xcb_window_t win)
{
  currentDesk = &desktops[currentDeskNum];
  // what does this need to do?
  client *new = malloc(sizeof(client));
  /* first we have to create a new client for the window */
  /* client->window is the win */
  new->x = 0;
  new->y = 0;
  new->width = 0;
  new->height = 0;
  new->id = currentDesk->wins;
  new->win = win;
  /* client->next will be null */
  new->next = NULL;
  /* client-> previouswill be the last element in the desktop client list */
  /* get last client */
  
  if (NULL == currentDesk->head)
  {
    new->prev = NULL;
    currentDesk->head = new;
    currentDesk->end = new;
  } else {
    new->prev = currentDesk->end;
    currentDesk->end->next = new;
    currentDesk->end = new;
  }
  currentDesk->current = new;
  // now iter should be the last client
  /* make the client->prev this last client */

  /* then we have to check if the currentdesktop has a head,  */
  /* if not then this client becomes the nhead. and the current client */

  /* if it has a head then we deal with the current client of the desktop */
  /* if there is a head then the current client of the desktop  */
  /* free(new); */
  debug("Client id %d\nhead id %d\nend id %d\ncurrent id %d\n",
	new->id,
	currentDesk->head->id,
	currentDesk->end->id,
	currentDesk->current->id);
  currentDesk->wins++;
}

void maprequest(xcb_generic_event_t *e)
{
  debug("MAP REQUEST");
  /* grab the mouse pointer so the window will be opened at its location */

  xcb_map_request_event_t *ev = (xcb_map_request_event_t *) e;
  xcb_window_t window = ev->window;
  xcb_query_pointer_reply_t *pointer;
  pointer = xcb_query_pointer_reply(conn, xcb_query_pointer(conn, window), 0);
  debug("Pointer %d %d", pointer->win_x, pointer->win_y);

  if (!(change))
    addwin(window);
    
  if (pointer == NULL) {
    uint32_t values[3] = { 0, 0, 10 };
  } else {
    /* uint32_t values[3] = { pointer->win_x, pointer->win_y, 10 }; */
    uint32_t values[3] = {point_x, point_y, 10};
  }
  free(pointer);
  uint32_t MASKS = XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_BORDER_WIDTH;

  xcb_configure_window(conn, window, MASKS, values);
  /* xcb_configure_window(conn, window, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y | XCB_CONFIG_WINDOW_BORDER_WIDTH, values); */
  xcb_flush(conn);

  uint32_t windowArgs[1] = {XCB_EVENT_MASK_EXPOSURE       |
			    XCB_EVENT_MASK_ENTER_WINDOW   | XCB_EVENT_MASK_LEAVE_WINDOW   |
			    XCB_EVENT_MASK_KEY_PRESS      | XCB_EVENT_MASK_KEY_RELEASE    };
  
  /* uint32_t windowArgs[1] = { XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE | XCB_EVENT_MASK_POINTER_MOTION_HINT }; */
  xcb_change_window_attributes_checked(conn, window, XCB_CW_EVENT_MASK, windowArgs);
  xcb_flush(conn);
  
  xcb_map_window(conn, window);
  xcb_flush(conn);

  xcb_warp_pointer(conn, XCB_NONE, window, 0, 0, 0, 0, 50, 50);
  xcb_flush(conn);
  
}

// this will eventually setup the main event, haven't gotten around to refactoring it yet.
int setup()
{

  //create desktops
  for (int i = 0; i < NUMBER_OF_DESKTOPS; i++)
  {
    desktop *new = malloc(sizeof(desktop));
    new->head = NULL;
    new->current = NULL;
    new->prevfocus = NULL;
    new->end = NULL;
    new->wins = 0;
    desktops[i] = *new;
    debug("%d", i);
    free(new);
  }

  currentDeskNum = 0;
  return 0;
}

void setupKeys() {

}

int notRoot(xcb_window_t win)
{
  if (win != root)
    {
      return 0;
    }
  else
    {
      return 1;
    }
}

int main (int argc, char **argv)
{
  conn = xcb_connect(NULL, NULL);
  if (xcb_connection_has_error(conn)) return 1;

  screen = xcb_setup_roots_iterator(xcb_get_setup(conn)).data;
  root = screen->root;
  xcb_drawable_t root_visual = screen->root_visual;
  
  ww = screen->width_in_pixels;
  wh = screen->height_in_pixels;

  debug("setup");
  setup();
  xcb_void_cookie_t cookie;
  xcb_generic_error_t *error;

  int mask = XCB_CW_EVENT_MASK;
  /* uint32_t args[] = { XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT | XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_BUTTON_PRESS}; */
  
  uint32_t args[1] = { XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_STRUCTURE_NOTIFY | XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE };
  cookie = xcb_change_window_attributes_checked(conn, root, mask, args);
  error = xcb_request_check(conn, cookie);

  xcb_keycode_t *r = xcb_get_keycodes(XK_r);
  xcb_keycode_t *f = xcb_get_keycodes(XK_f);
  xcb_keycode_t *c = xcb_get_keycodes(XK_c);
  xcb_keycode_t *j = xcb_get_keycodes(XK_j);
  xcb_keycode_t *h = xcb_get_keycodes(XK_h);
  xcb_keycode_t *k = xcb_get_keycodes(XK_k);
  xcb_keycode_t *l = xcb_get_keycodes(XK_l);
  xcb_keycode_t *ent = xcb_get_keycodes(XK_Return);
  xcb_keycode_t *dot = xcb_get_keycodes(XK_period);
  xcb_keycode_t *comma = xcb_get_keycodes(XK_comma);
  xcb_keycode_t *tab = xcb_get_keycodes(XK_Tab);
  xcb_keycode_t *one = xcb_get_keycodes(XK_1);
  xcb_keycode_t *two = xcb_get_keycodes(XK_2);
  xcb_keycode_t *three = xcb_get_keycodes(XK_3);
  xcb_keycode_t *four = xcb_get_keycodes(XK_4);
  xcb_keycode_t *five = xcb_get_keycodes(XK_5);
  
  // grab just mod 4 keybindings
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4,  *c,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4,  *l,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4,  *ent,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4,  *r,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4,  *j,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4,  *k,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4,  *f,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4,  *h,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4, *dot,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4, *comma,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4, *tab,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4, *one,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4, *two,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4, *three,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4, *four,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4, *five,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);

  // grab combinations of mod4 and mod1 keybindings
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_1,  *l,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_1,  *h,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_1,  *j,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_1,  *k,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);

  // grab combination of mod4 and control keybinding
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_CONTROL,  *j,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_CONTROL,  *k,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);

  // grab shift control bindings
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT, *tab,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT, *one,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT, *two,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT, *three,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT, *four,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT, *five,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT, *h,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);
  xcb_grab_key(conn, 0, root, XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT, *l,
	       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC);

  //grab mouse buttons
  xcb_grab_button(conn, 0, root, XCB_EVENT_MASK_BUTTON_PRESS |
		  XCB_EVENT_MASK_BUTTON_RELEASE, XCB_GRAB_MODE_ASYNC,
		  XCB_GRAB_MODE_ASYNC, root, XCB_NONE, 1, XCB_MOD_MASK_4);

  xcb_grab_button(conn, 0, root, XCB_EVENT_MASK_BUTTON_PRESS |
		  XCB_EVENT_MASK_BUTTON_RELEASE, XCB_GRAB_MODE_ASYNC,
		  XCB_GRAB_MODE_ASYNC, root, XCB_NONE, 3, XCB_MOD_MASK_4);
  
  xcb_flush(conn);
    
  for (;;)
  {
    ev = xcb_wait_for_event(conn);

    switch (ev->response_type & ~0x80) {

    case XCB_KEY_PRESS:
    {
      xcb_key_press_event_t *e;
      e = ( xcb_key_press_event_t *) ev;
      debug("Key bress");
      win = e->child;
      if (e->detail == *c) {
	/* destroyWin(win); */
	xcb_destroy_window(conn, win);
	/* currentDesk->wins--; */
      } else if ((e->detail == *l) && (e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_1))) {
	resize(win, 0, 20, 0);
      } else if ((e->detail == *h) && (e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_1))) {
	resize(win, 0, 20, 1);
      } else if ((e->detail == *j) && (e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_1))) {
	resize(win, 1, 20, 0);
      } else if ((e->detail == *k) && (e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_1))) {
	resize(win, 1, 20, 1);
      } else if ((e->detail == *j) && (e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_CONTROL))) {
	resizeScale(win, 0);
      } else if ((e->detail == *k) && (e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_CONTROL))) {
	resizeScale(win, 1);
      } else if ((e->detail == *h) && e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT)) {
	tile(win, 1); 
      } else if ((e->detail == *l) && e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT)) {
	tile(win, 0);
      } else if (e->detail == *ent) {
	spawn("/usr/bin/sakura");
      } else if (e->detail == *r) {
	spawn("/usr/bin/dmenu_run");
      } else if (e->detail == *j) {
	move(win, 1, MOVESTEP, 0);
      } else if (e->detail == *k) {
	move(win, 1, MOVESTEP, 1);
      } else if (e->detail == *l) {
	move(win, 0, MOVESTEP, 0);
      } else if (e->detail == *h) {
	move(win, 0, MOVESTEP, 1);
      } else if (e->detail == *f) {
	fullscreenWindow(win);
      } else if (e->detail == *dot) {
	xcb_lower_window(conn, win);
      } else if (e->detail == *comma) {
	xcb_raise_window(conn, win);
      } else if ((e->detail == *tab) && e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT)) {
	prevwin();
      } else if (e->detail == *tab) {
	nextwin();
      } else if ((e->detail == *one) && e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT)) {
	changeDesktop(0);
      } else if ((e->detail == *two) && e->state == (XCB_MOD_MASK_4 | XCB_MOD_MASK_SHIFT)) {
	changeDesktop(0);
      } else if ((e->detail == *one) && e->state == XCB_MOD_MASK_4) {
	changeDesktop(0);
      } else if ((e->detail == *two) && e->state == XCB_MOD_MASK_4) {
	changeDesktop(1);
      } else if ((e->detail == *three) && e->state == XCB_MOD_MASK_4) {
	changeDesktop(2);
      } else if ((e->detail == *four) && e->state == XCB_MOD_MASK_4) {
	changeDesktop(3);
      } else if ((e->detail == *five) && e->state == XCB_MOD_MASK_4) {
	changeDesktop(4);
      }
      xcb_flush(conn);
      break;
     
    }


    case XCB_KEY_RELEASE:
    {
      xcb_ungrab_pointer(conn, XCB_CURRENT_TIME);
      xcb_flush(conn);
      break;
    }

    case XCB_BUTTON_PRESS:
    {
 
      xcb_button_press_event_t *e;
      e = ( xcb_button_press_event_t *) ev;
      if (!(e->child))
	break;
      win = e->child;
      values[0] = XCB_STACK_MODE_BELOW;
      xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_STACK_MODE, values);
      geom = xcb_get_geometry_reply(conn, xcb_get_geometry(conn, win), NULL);

      if (1 == e->detail) {
	values[2] = 1;
	    
	xcb_warp_pointer(conn, XCB_NONE, win, 0, 0, 0, 0, 1, 1);
      } else {
	values[2] = 3;
	xcb_warp_pointer(conn, XCB_NONE, win, 0, 0, 0, 0, geom->width, geom->height);
	    
      }
      xcb_grab_pointer(conn, 0, root, XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_BUTTON_MOTION | XCB_EVENT_MASK_POINTER_MOTION_HINT,
		       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC, root, XCB_NONE, XCB_CURRENT_TIME);

      xcb_flush(conn);
    }
    break;
    
    case XCB_MOTION_NOTIFY:
    {
      xcb_motion_notify_event_t *e = ( xcb_motion_notify_event_t *) ev;
      xcb_query_pointer_reply_t *pointer;
      pointer = xcb_query_pointer_reply(conn, xcb_query_pointer(conn, root), 0);
      point_x = e->event_x;
      point_y = e->event_y;
      if (values[2] == 1) {/* move */
	geom = xcb_get_geometry_reply(conn, xcb_get_geometry(conn, win), NULL);
	values[0] = (pointer->root_x + geom->width > screen->width_in_pixels)?
	  (screen->width_in_pixels - geom->width):pointer->root_x;
	values[1] = (pointer->root_y + geom->height > screen->height_in_pixels)?
	  (screen->height_in_pixels - geom->height):pointer->root_y;
	xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, values);
	xcb_flush(conn);
      } else if (values[2] == 3) { /* resize */
	geom = xcb_get_geometry_reply(conn, xcb_get_geometry(conn, win), NULL);
	values[0] = pointer->root_x - geom->x;
	values[1] = pointer->root_y - geom->y;
	xcb_configure_window(conn, win, XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT, values);
	xcb_flush(conn);
      }
    }
    break;

    case XCB_ENTER_NOTIFY:
    {
      xcb_enter_notify_event_t *enter = (xcb_enter_notify_event_t *) ev;
      /* current = enter->event; */
      /* *currentClient = findClient(enter->event); */
      //should take input focus to follow the mouse
      if (enter->event == root) {
	xcb_set_input_focus(conn, XCB_NONE, XCB_INPUT_FOCUS_POINTER_ROOT, XCB_CURRENT_TIME);
      } else {
	xcb_set_input_focus(conn, XCB_INPUT_FOCUS_POINTER_ROOT, enter->event, XCB_CURRENT_TIME);
      }
      /* xcb_raise_window(conn, enter->event); */
      xcb_flush(conn);
    }
    break;
    
    case XCB_MAP_NOTIFY:
    {
      xcb_generic_event_t *e = (xcb_generic_event_t *) ev;
      maprequest(e);
    }
    break;

    case XCB_BUTTON_RELEASE:
    {
      xcb_ungrab_pointer(conn, XCB_CURRENT_TIME);
      xcb_flush(conn);
    }
    break;
    }
  }
  free(desktops);

  return 0;
}
