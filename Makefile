CC=clang
OPTS=-lxcb
#CFLAGS= -std=c99 -pedantic -Wall -Wextra
LIBS=-lxcb -lxcb-keysyms -lxcb-util
OUTPUT= -o tomwm
# all:
# 	${CC} tomwm.c -o tomwm ${LIBS} 
tomwm:
	${CC} tomwm.c -o tomwm ${LIBS} 
tinywm:
	${CC} tinywm.c -o tomwm ${LIBS}
clean:
	rm -f tomwm
	rm -f *~
install:
	@echo install tomwm to /usr/bin
	@install -Dm755 tomwm /usr/bin/tomwm
