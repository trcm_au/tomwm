#include <stdio.h>
#include <xcb/xcb.h>

int main()
{
  int i, screenNum;
  xcb_connection_t *connection = xcb_connect(NULL, &screenNum);

  const xcb_setup_t *setup = xcb_get_setup(connection);
  xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);
  for (int i = 0; i < screenNum; ++i)
  {
    xcb_screen_next(&iter);
  }
  xcb_screen_t *screen = iter.data;

  printf("%d\n", screen->width_in_pixels);
  printf("%d\n", screen->height_in_pixels);

  xcb_window_t window = xcb_generate_id(connection);
  xcb_create_window( connection,
		     XCB_COPY_FROM_PARENT,
		     window,
		     screen->root,
		     0, 0,
		     screen->width_in_pixels - 20, screen->height_in_pixels - 20,
		     10,
		     XCB_WINDOW_CLASS_INPUT_OUTPUT,
		     screen->root_visual,
		     0, NULL);
  xcb_map_window(connection, window);
  xcb_flush(connection);
  pause();
  xcb_disconnect(connection);
  return 0;
}

