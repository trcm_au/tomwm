#include <stdio.h>
#include <stdlib.h>


struct node {
  struct node *next;
  struct node *prev;
  int data;
};

struct node *head;
struct node *end;

int addNode(int data)
{
  struct node *prevNode = malloc(sizeof(struct node));

  struct node *new = malloc(sizeof(struct node));
  new->data = end->data + 1;
  new->prev = end;
  new->next = NULL;
  end->next = new;
  end = new;
  

  return 0;
}

int printList()
{
  struct node *curr;
  curr = head;
  for(;;)
  {
    printf("%d", curr->data);
    if (curr->next == NULL)
      break;
    curr = curr->next;
  }
  printf("\n");
  return 0;
}

int printBackwards()
{
  struct node *curr;
  curr = end;
  for(;;)
  {
    printf("%d", curr->data);
    if (curr->prev == NULL)
      break;
    curr = curr->prev;
  }
  printf("\n");
  return 0;
}

int deleteNode(int nodeData)
{
	struct node *curr;
	curr = head;
	for(;;)
	{
		if (curr->data == nodeData)
		{
			if (curr->next != NULL && curr->prev != NULL)
			{
				curr->next->prev = curr->prev;
				curr->prev->next = curr->next;
			}
			
		}
		if (curr->next == NULL)
			break;
		curr = curr->next;
	}
	free(curr);
	return 1;
}

int main() {
  head = malloc(sizeof(struct node));
  head->prev = NULL;
  head->data = 1;
  end = malloc(sizeof(struct node));
  end = head;
  addNode(1);
  addNode(1);
  addNode(1);

  printList();
  
  printBackwards();
  deleteNode(2);
  printList();
  return 0;
}

